<!DOCTYPE html>
<html>
<head>
<title>Data Pasien</title>
<!--Bagian CSS untuk Styling Tabel-->
<style type="text/css">
          table, th, td
          {
               border: 1px solid black;
          }
</style>
</head>
<body>
 
<h3>Data Pasien</h3>
<?php
// untuk meload data xml (pasien.xml) dengan cara SimpleXML 
$pasien = new SimpleXMLElement('http://localhost/parsingXML/pasien.xml', null, true);
 
// menampilkan data ke XML ke dalam tabel HTML
echo "
<table>
<tr>
<th>Nomer Rekam Medis</th>
<th>Nama</th>
<th>Alamat</th>
<th>Kecamatan</th>
<th>Telp</th>
</tr>
 
";
 
// melakukan looping penampilan data pasien
foreach($pasien as $id)
{
        echo "
<tr>
<td width='200'>{$id->rekam}</td>
<td width='200'>{$id->nama}</td>
<td width='200'>{$id->alamat}</td>
<td width='130'>{$id->kecamatan}</td>
<td width='80'>{$id->telp}</td>
</tr>
 
";
}
echo '</table>';
?>
<br>
</br>
<button onclick="window.print()">Cetak Data Pasien</button>
 
</body>
</html>